# 快速上手

为了帮助开发者迅速掌握TinyEngine并深入理解其核心特性，我们设计了一个简单的实践上手案例：“开关按钮控制图片显示与隐藏”。
这个案例直观展示了TinyEngine在交互设计，允许用户通过一个简单的开关按钮来控制图片的显示与隐藏状态。  

通过这个案例，开发者可以快速学习如何利用TinyEngine的低代码特性，实现界面交互逻辑。

## 效果图

![案例的效果图](./imgs/showImg.png)

## 第一步，新建页面

![步骤1 - 新建页面的操作界面](./imgs/page1.png)

## 第二步，拖入元素

![步骤2 - 拖入元素的操作演示](./imgs/addComponent.png)

## 第三步，设置状态变量

![步骤3 - 设置状态变量的配置界面](./imgs/addState.png)

## 第四步，设置文本和图片地址：

修改text属性，修改文本为：“图片显示开关”
设置src属性，修改图片地址为：`https://res.hc-cdn.com/lowcode-portal/1.1.65/img/home/top-banner.jpg`

![步骤4.1 - 设置文本属性](./imgs/imgswitch.png)
![步骤4.2 - 设置图片地址](./imgs/setImgSrc.png)

## 第五步，设置图片样式和绑定显示变量

![步骤5.1 - 设置图片样式](./imgs/setImgSty.png)
![步骤5.2 - 绑定显示变量](./imgs/setImgSta.png)

## 第六步，给开关绑定事件

![步骤6 - 绑定事件配置界面](./imgs/bangEnv.png)

## 第七步，绑定点击事件并预览

![步骤7 - 绑定点击事件并预览](./imgs/preImgChange.png)

## 恭喜你，完成了这个小案例。


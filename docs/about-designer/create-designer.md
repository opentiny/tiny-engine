# 关于设计器

## 如何创建一个设计器

用户可以在 [我的设计器](https://www.opentiny.design/tiny-engine#/my-platform) 中创建设计器，创建设计器 &rarr; 填写必要的字段 &rarr; 确定
![create-platform]( ./imgs/createPlatform.png)

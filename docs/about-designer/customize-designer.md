# 关于设计器

## 如何定制一个设计器

物料资产包、主题、工具、插件和DSL定制完成后，有两种方式可以构建设计器。

方式一：可视化构建

![build-platform1]( ./imgs/buildPlatform1.png)

方式二：由源码构建

![build-platform2]( ./imgs/buildPlatform2.png)
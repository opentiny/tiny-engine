# API 总览

TinyEngine的API主要分为`TinyEngine能力API`和`开源后端接口API`。

## TinyEngine能力API
主要提供以下几类API
- 主包API
- 画布API
- 布局API
- 物料API
- 属性面板API


## 后端接口API
主要提供以下几类API
- AI功能接口
- 应用管理
- 区块分类
- 应用工具类管理
- 区块管理
- 数据源管理
- DSL代码生成
- 国际化管理
- 物料中心
- 页面管理
- APP服务


## API约定
### 元服务API

所有的元服务，全都为use开头的驼峰式命名。如我们内置的useMaterial,useCanvas等。

## 声明
以上API处于公测阶段，具体API可能会根据实际需求有所调整


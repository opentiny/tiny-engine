# 查看大纲树

在左侧插件栏中，单击![大纲树插件图标](./imgs/icon-tree.png)，展开并查看页面大纲树。

   **图 1**  设置主页
   ![设置主页](not-found/setIndex-20.png)


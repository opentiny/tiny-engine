// 描述该插件的相关元信息
export default {
  id: 'engine.theme.custom',
  text: '自定义主题',
  type: 'custom',
  icon: 'dark'
}
